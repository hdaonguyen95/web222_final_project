/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const validate_user_name = (name = '') => {
  return new Promise((resolve, reject) => {
    if (name.match(/[A-Za-z' ']/gi).length !== name.length) {
      reject('Invalid name!\nThe user name must contain only letters.');
    }
    resolve(true);
  });
};

const validate_address = (address = '') => {
  return new Promise((resolve, reject) => {
    if (
      address.match(/[A-Za-z' ']/gi) !== null &&
      address.match(/[A-Za-z' ']/gi).length === address.length
    ) {
      reject('Invalid address!\nMissing street number.');
    }

    if (
      address.match(/[0-9' '-]/gi) !== null &&
      address.match(/[0-9' '-]/gi).length === address.length
    ) {
      reject('Invalid address!\nMissing street name.');
    }

    if (address.match(/[A-Za-z0-9' '-]/gi).length !== address.length) {
      reject(
        'Invalid address!\nThe address must contain only letters, numbers, space and the symbol "-".'
      );
    }
    resolve(true);
  });
};

const validate_postal_code = (postalCode = '') => {
  return new Promise((resolve, reject) => {
    if (!postalCode.match(/\w\d\w\d\w\d/gi)) {
      reject('Invalid postal code!');
    }
    resolve(true);
  });
};

const validateContactForm = () => {
  return new Promise(resolve => {
    let user_fullname = document.getElementById('user_fullname').value;
    let user_address = document.getElementById('user_address').value;
    let user_postal_code = document.getElementById('user_postal_code').value;
    let error_message_container = document.querySelector('.error_message');

    error_message_container.addEventListener('click', () => {
      error_message_container.classList.remove('error_message_active');
    });

    error_message_container.innerHTML = '';
    error_message_container.classList.add('error_message_active');
    validate_user_name(user_fullname)
      .then(nameResult => {
        if (nameResult) {
          validate_address(user_address)
            .then(addressResult => {
              if (addressResult) {
                validate_postal_code(user_postal_code)
                  .then(postalCodeResult => {
                    if (postalCodeResult) {
                      error_message_container.classList.remove('error_message_active');
                      resolve(true);
                    }
                  })
                  .catch(err => {
                    let p = document.createElement('p');
                    p.innerHTML = err;
                    error_message_container.appendChild(p);
                    error_message_container.classList.add('error_message_active');
                    resolve(false);
                  });
              }
            })
            .catch(err => {
              let p = document.createElement('p');
              p.innerHTML = err;
              error_message_container.appendChild(p);
              error_message_container.classList.add('error_message_active');
              resolve(false);
            });
        }
      })
      .catch(err => {
        let p = document.createElement('p');
        p.innerHTML = err;
        error_message_container.appendChild(p);
        error_message_container.classList.add('error_message_active');
        resolve(false);
      });
  });
};

window.onload = () => {
  const burgerButton = document.getElementById('burgerBtn');
  const sidebar_container = document.getElementById('sidebar_container');
  const highlighted_products_container = document.querySelector('.highlighted_products_container');

  burgerButton.addEventListener('click', () => {
    setTimeout(() => {
      highlighted_products_container.classList.toggle('highlighted_products_container_hide');
    }, 500);
    sidebar_container.classList.toggle('sidebar_container_active');
  });

  let contact_form_main = document.querySelector('.contact_form_main');
  contact_form_main.onsubmit = event => {
    validateContactForm().then(result => {
      if (!result) {
        event.preventDefault();
      }
    });
  };

  let order_number_container = document.querySelector('.order_number_container');
  order_number_container.classList.add('order_number_container_hide');
  let order_number_input = document.getElementById('order_number_input');
  order_number_input.required = false;

  let order_problem_checkbox = document.getElementById('order_problem');
  let comment_checkbox = document.getElementById('comment');
  let question_checkbox = document.getElementById('question');

  order_problem_checkbox.onclick = event => {
    order_number_container.classList.remove('order_number_container_hide');
    order_number_input.required = true;
  };

  comment_checkbox.onclick = event => {
    order_number_container.classList.add('order_number_container_hide');
    order_number_input.required = false;
  };

  question_checkbox.onclick = event => {
    order_number_container.classList.add('order_number_container_hide');
    order_number_input.required = false;
  };
};
