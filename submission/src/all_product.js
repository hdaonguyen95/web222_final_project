/* eslint-disable no-undef */
window.onload = () => {
  const burgerButton = document.getElementById('burgerBtn');
  const sidebar_container = document.getElementById('sidebar_container');
  const products_container = document.querySelector('.products_container');

  burgerButton.addEventListener('click', () => {
    setTimeout(() => {
      products_container.classList.toggle('products_container_hide');
    }, 500);
    sidebar_container.classList.toggle('sidebar_container_active');
  });

  // Make the container of div to hold class row
  // Each div will represent a row
  let productsList = getProductByType(getTypeOfProductFromUrl());
  let numberOfRow = Math.ceil(productsList.length / 2);
  let product_main = document.querySelector('.products_main');
  let index = 0;
  for (let rowCounter = 0; rowCounter < numberOfRow - 1; ++rowCounter) {
    let productRow = document.createElement('div');
    productRow.classList.add('row');
    for (let colCounter = 0; colCounter < 2; ++colCounter) {
      let productColumn = document.createElement('div');
      productColumn.classList.add('col-lg-6');
      productColumn.classList.add('col-md-12');
      productColumn.classList.add('col-sm-12');
      productColumn.classList.add('single_product');

      let img = document.createElement('img');
      img.setAttribute('src', productsList[index].img);
      img.setAttribute('alt', productsList[index].device_name);

      let product_name = document.createElement('p');
      product_name.classList.add('product_name');
      product_name.innerHTML = productsList[index].device_name;

      let product_price = document.createElement('p');
      product_price.classList.add('product_price');
      product_price.innerHTML = productsList[index].price;

      let buying_option_btn_group = document.createElement('div');
      buying_option_btn_group.classList.add('buying_option_btn_group');

      let buyButton = document.createElement('button');
      buyButton.classList.add('buy_btn');
      let buyButtonLink = document.createElement('a');
      buyButtonLink.setAttribute('href', '/product_view.html?id=' + productsList[index].id);
      buyButton.innerHTML = 'View Product';
      buyButtonLink.appendChild(buyButton);

      let addToCartButton = document.createElement('button');
      addToCartButton.classList.add('add_to_cart_btn');
      addToCartButton.innerHTML = 'Add to Cart';

      buying_option_btn_group.appendChild(buyButtonLink);
      buying_option_btn_group.appendChild(addToCartButton);

      productColumn.appendChild(img);
      productColumn.appendChild(product_name);
      productColumn.appendChild(product_price);
      productColumn.appendChild(buying_option_btn_group);

      productRow.appendChild(productColumn);
      ++index;
    }
    product_main.appendChild(productRow);
  }

  let productRow = document.createElement('div');
  productRow.classList.add('row');
  for (let colCounter = index; colCounter < productsList.length; ++colCounter) {
    let productColumn = document.createElement('div');
    productColumn.classList.add('col-lg-6');
    productColumn.classList.add('col-md-12');
    productColumn.classList.add('col-sm-12');
    productColumn.classList.add('single_product');

    let img = document.createElement('img');
    img.setAttribute('src', productsList[index].img);
    img.setAttribute('alt', productsList[index].device_name);

    let product_name = document.createElement('p');
    product_name.classList.add('product_name');
    product_name.innerHTML = productsList[index].device_name;

    let product_price = document.createElement('p');
    product_price.classList.add('product_price');
    product_price.innerHTML = productsList[index].price;

    let buying_option_btn_group = document.createElement('div');
    buying_option_btn_group.classList.add('buying_option_btn_group');

    let buyButton = document.createElement('button');
    buyButton.classList.add('buy_btn');
    let buyButtonLink = document.createElement('a');
    buyButtonLink.setAttribute('href', '/product_view.html?id=' + productsList[index].id);
    buyButton.innerHTML = 'View Product';
    buyButtonLink.appendChild(buyButton);

    let addToCartButton = document.createElement('button');
    addToCartButton.classList.add('add_to_cart_btn');
    addToCartButton.innerHTML = 'Add to Cart';

    buying_option_btn_group.appendChild(buyButtonLink);
    buying_option_btn_group.appendChild(addToCartButton);

    productColumn.appendChild(img);
    productColumn.appendChild(product_name);
    productColumn.appendChild(product_price);
    productColumn.appendChild(buying_option_btn_group);

    productRow.appendChild(productColumn);
    ++index;
  }
  product_main.appendChild(productRow);
};

const getTypeOfProductFromUrl = () => {
  let url = window.location.href;
  let urlComponents = url.split('?');
  let type = urlComponents[1].split('=');
  return type[1];
};

const getProductByType = (type = '') => {
  return products_database.filter(product => {
    return product.type.toLowerCase() === type.toLowerCase();
  });
};
