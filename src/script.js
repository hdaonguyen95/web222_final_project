/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
window.onload = () => {
  const burgerButton = document.getElementById('burgerBtn');
  const sidebar_container = document.getElementById('sidebar_container');
  const highlighted_products_container = document.querySelector('.highlighted_products_container');

  burgerButton.addEventListener('click', () => {
    setTimeout(() => {
      highlighted_products_container.classList.toggle('highlighted_products_container_hide');
    }, 500);
    sidebar_container.classList.toggle('sidebar_container_active');
  });
};

const getComputerByID = (id = '') => {
  return products_database.filter(product => {
    return product.id === id;
  });
};

const getProductIdFromUrl = () => {
  let url = window.location.href;
  let urlComponents = url.split('?');
  let id = urlComponents[1].split('=');
  return id[1];
};
