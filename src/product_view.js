/* eslint-disable no-undef */
//Product View Page
let productToRender = getComputerByID(getProductIdFromUrl());
if (productToRender !== undefined) {
  let productName = document.querySelector('.product_name');
  let productNameContent = document.createElement('p');
  productNameContent.innerHTML = productToRender[0].device_name;
  productName.appendChild(productNameContent);

  let productImage = document.querySelector('.product_img>img');
  productImage.setAttribute('src', productToRender[0].img);
  productImage.setAttribute('alt', productToRender[0].device_name);

  let productDescription = document.querySelector('.product_description');
  let productDescriptionContent = document.createElement('p');
  productDescriptionContent.innerHTML = productToRender[0].overview;
  productDescription.appendChild(productDescriptionContent);

  let productPrice = document.querySelector('.product_price');
  productPrice.innerHTML = productToRender[0].price;

  let product_display = document.querySelector('.product_display');
  product_display.innerHTML = productToRender[0].display;

  let product_processor = document.querySelector('.product_processor');
  product_processor.innerHTML = productToRender[0].processor;

  let number_of_core = document.querySelector('.number_of_core');
  number_of_core.innerHTML = productToRender[0].number_of_core;

  let processor_speed = document.querySelector('.processor_speed');
  processor_speed.innerHTML = productToRender[0].processor_speed;

  let product_storage = document.querySelector('.product_storage');
  product_storage.innerHTML = productToRender[0].storage;

  let ram_size = document.querySelector('.ram_size');
  ram_size.innerHTML = productToRender[0].ram_size;

  let product_graphics = document.querySelector('.product_graphics');
  product_graphics.innerHTML = productToRender[0].graphics_card;

  let product_port = document.querySelector('.product_port');
  product_port.innerHTML = productToRender[0].usb_ports;

  let product_wifi = document.querySelector('.product_wifi');
  product_wifi.innerHTML = productToRender[0].wifi;

  let product_bluetooth = document.querySelector('.product_bluetooth');
  product_bluetooth.innerHTML = productToRender[0].bluetooth;
}

const specs_button = document.querySelector('.specs_button');
const information_field = document.getElementById('information_field');
const return_policy_button = document.querySelector('.return_policy_button');
const return_policy_field = document.getElementById('return_policy_field');

return_policy_field.classList.add('hide_return_policy');

specs_button.addEventListener('click', () => {
  return_policy_field.classList.add('hide_return_policy');
  information_field.classList.remove('hide_product_info');
});

return_policy_button.addEventListener('click', () => {
  return_policy_field.classList.remove('hide_return_policy');
  information_field.classList.add('hide_product_info');
});
